import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css'; 
import '@sweetalert2/themes/dark/dark.css';

window.$ = window.jQuery = require('jquery');
Vue.config.productionTip = false
Vue.use(VueSweetalert2);
Vue.use(VueMoment, {moment});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
