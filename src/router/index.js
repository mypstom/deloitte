import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Active from '../views/Active.vue'
import SignUp from '../views/SignUp.vue'
import Machines from '../views/Machines.vue'
import MachineList from '../views/MachineList.vue'
import Machine from "../views/Machine.vue"
import User from '../views/User.vue'
import Setting from '../views/Setting.vue'
import Profile from '../views/Profile.vue'
import Vpn from '../views/Vpn.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/active",
    name: "active",
    component: Active
  },
  {
    path: "/signup",
    name: "signup",
    component: SignUp
  },
  {
    path: "/machines",
    name: "machines",
    component: Machines,
    children: [
      // { path: "/:id", component: Machine, props: true },
      { path: "/", component: MachineList }
    ]
  },
  {
    path: "/machine",
    name: "machine",
    component: Machine
  },
  {
    path: "/user/:id",
    name: "user",
    component: User
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile
  },
  {
    path: "/setting",
    name: "setting",
    component: Setting
  },
  {
    path: "/vpn",
    name: "vpn",
    component: Vpn
  },
  {
    path: '*',
    name: "fallback",
    component: Home
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
