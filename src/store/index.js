import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo: {},
    userName: "",
    isPageLoading: true
  },
  mutations: {
    updateUserInfo: function(state, data){
      state.userInfo = data;
    },
    updateUserName: function(state, data){
      state.userName = data;
    },
    setPageLoading: function(state, data){
      state.isPageLoading = data;
    }
  },
  actions: {
  },
  modules: {
  }
})
